#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

bool running = true;
void stop_handler(int sig)
{
  printf("Inside STOP_HANDLER\n");
  printf("Numero sig recu: %d\n", sig);
  running = false;
};

void exit_message()
{
  printf("EXIT MESSAGE\n");
}

int main()
{
  srand(time(NULL));
  // atexit
  atexit(exit_message);

  // sigaction
  struct sigaction psa;
  psa.sa_handler = stop_handler;

  sigaction(SIGINT, &psa, NULL);
  sigaction(SIGTERM, &psa, NULL);
  sigaction(SIGPIPE, &psa, NULL);

  // fifo
  printf("Opening the fifo...\n");
  int fd = open("myfifo", O_WRONLY);
  if (fd == -1)
  {
    printf("Error opening the fifo\n");
    exit(EXIT_FAILURE);
  }

  int data[3] = {getpid(), getppid(), rand() % 100};
  // Start of loop message
  printf("Avant la boucle infinie du serveur.\n");
  while (running)
  {
    printf("writing\n");
    if (write(fd, &data, 3 * sizeof(int)) == -1)
    {
      if (errno == EPIPE)
      {
        printf("Reading end closed: breaking out of the loop with sigpipe handling.\n");
      }
      else
      {
        printf("Error writing on the file descriptor: exiting.\n");
        exit(EXIT_FAILURE);
      }
    };
    sleep(1);
  }
  close(fd);

  // End of loop message
  printf("Après la boucle infinie du serveur\n");

  exit(EXIT_SUCCESS);
}
