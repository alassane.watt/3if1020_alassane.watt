#include <vector>
#include <iostream>
#include <functional>
#include <forward_list>
#include <limits>
// #include <stdio.h>
// #include <unistd.h>
using namespace std;

// ********************************************************** Part 1 **********************************************************
void print_tab(const vector<int> &tab)
{
  for (int i = 0; i < tab.size(); i++)
  {
    printf("%d\n", tab[i]);
  }
}

void random_tab(vector<int> &tab)
{
  for (int i = 0; i < tab.size(); i++)
  {
    tab[i] = rand() % 100;
  }
}

void sort_tab_1(vector<int> &tab)
{
  int i, j, min_idx;
  int n = tab.size();
  for (i = 0; i < n - 1; i++)
  {
    min_idx = i;
    for (j = i + 1; j < n; j++)
      if (tab[j] < tab[min_idx])
        min_idx = j;

    swap(tab[min_idx], tab[i]);
  }
}

void test_1()
{
  vector<int> tab(10);
  random_tab(tab);
  sort_tab_1(tab);
  print_tab(tab);
}

// Fonctions de comparaison
int petit(int i, int j)
{
  return (i <= j) ? 1 : 0;
}

int grand(int i, int j)
{
  return (i >= j) ? 1 : 0;
}

// sort_tab_2 using pointers
void sort_tab_2_pointer(vector<int> &tab, int (*comparator)(int, int))
{
  int i, j, min_idx;
  int n = tab.size();

  for (i = 0; i < n - 1; i++)
  {
    min_idx = i;
    for (j = i + 1; j < n; j++)
      if ((*comparator)(tab[j], tab[min_idx]))
        min_idx = j;

    swap(tab[min_idx], tab[i]);
  }
}

//
void sort_tab_2(vector<int> &tab, std::function<int(int, int)> comparator)
{
  int i, j, min_idx;
  int n = tab.size();

  for (i = 0; i < n - 1; i++)
  {
    min_idx = i;
    for (j = i + 1; j < n; j++)
      if (comparator(tab[j], tab[min_idx]))
        min_idx = j;

    swap(tab[min_idx], tab[i]);
  }
}

void test_2()
{
  vector<int> tab(10);
  random_tab(tab);
  // sort_tab_2_pointer(tab, grand);
  sort_tab_2(tab, petit);
  print_tab(tab);
}

// ********************************************************** Part 2   **********************************************************

// random list
forward_list<int> random_list(int n)
{
  forward_list<int> list;
  for (int i = 0; i < n; i++)
  {
    list.push_front(rand() % 100);
  }
  return list;
}

// print list
void print_list(const forward_list<int> &list)
{
  printf("( ");
  for (int i : list)
  {
    printf("%d ", i);
  }
  printf(")\n");
}

// map function
forward_list<int> map(const forward_list<int> &list, function<int(int)> func)
{
  forward_list<int> mappedList;
  for (int i : list)
  {
    mappedList.push_front(func(i));
  }
  return mappedList;
}

forward_list<int> filter(const forward_list<int> &list, std::function<bool(int)> func)
{
  forward_list<int> filteredList;
  for (int i : list)
  {
    if (func(i))
    {
      filteredList.push_front(i);
    }
  }
  return filteredList;
}

// test_3
void test_3()
{
  forward_list<int> list = random_list(10);
  print_list(list);
  int coef = 1 + rand() % 5;
  printf("Coefficient de multiplication de map: %d\n", coef);
  forward_list<int> mappedList = map(list, [coef](int i) { return i * coef; });
  forward_list<int> filteredList = filter(mappedList, [](int k) { return (k % 2 == 0) ? 1 : 0; });
  // print_list(mappedList);
  print_list(filteredList);

  // La liste initiale et et la liste resultat ont un ordre inverse. En effet la liste a une methode d'implementation LIFO.
}

// Part 2.4
// original reduce func
int reduce(const forward_list<int> &list, int i0, function<int(int, int)> func)
{
  int res = i0;
  for (int i : list)
  {
    res = func(res, i);
  }
  return res;
}

// Min max reduce func
pair<int, int> reduce_min_max(const forward_list<int> &list)
{
  int m = std::numeric_limits<int>::max();
  int M = std::numeric_limits<int>::min();
  reduce(list, 0, [&m, &M](int a, int b) {
    m = (m > b) ? b : m;
    M = (M < b) ? b : M;
    return b;
  });

  return make_pair(m, M);
}

void test_4()
{
  forward_list<int> list = random_list(10);
  print_list(list);
  pair<int, int> p = reduce_min_max(list);
  printf("Min element is: %d\n", p.first);
  printf("Max element is: %d\n", p.second);
}

int main()
{
  srand(time(NULL));
  // test_1();
  // test_2();
  // test_3();
  test_4();
  return 0;
}
