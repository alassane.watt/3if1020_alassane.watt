#include "Expression.hpp"
#include <string>
#include <string.h>
#include <iostream>
#include <sstream>
using namespace std;

int Expression::nb_instances_ = 0;

Expression::Expression() { nb_instances_ += 1; }

Expression::~Expression()
{
  nb_instances_ -= 1;
}

ostream &operator<<(ostream &out, const Expression &expr)
{
  return expr.affiche(out);
}
Expression *Expression::simplify() const
{
  return clone();
}

Nombre::Nombre(int value) : value_(value) {}
Nombre::Nombre() : Nombre(0) {}
ostream &Nombre::affiche(ostream &out) const
{
  out << value_;
  return out;
}
Nombre *Nombre::derive(const string &var) const
{
  return new Nombre();
}
Nombre *Nombre::clone() const
{
  return new Nombre(value_);
}
Nombre *Nombre::simplify() const
{
  return this->clone();
}

Variable::Variable(string name) : name_(name) {}
ostream &Variable::affiche(ostream &out) const
{
  out << name_;
  return out;
}
Nombre *Variable::derive(const string &var) const
{
  if (var == name_)
  {
    return new Nombre(1);
  }
  else
  {
    return new Nombre();
  }
}

Variable *Variable::clone() const
{
  return new Variable(name_);
}
Variable *Variable::simplify() const
{
  return this->clone();
}

Operation::Operation(const Expression *ex_1, const Expression *ex_2, string symb) : symb_(symb)
{
  ex_g = ex_1->clone();
  ex_d = ex_2->clone();
}
Operation::~Operation()
{
  delete ex_g;
  delete ex_d;
}

ostream &Operation::affiche(ostream &out) const
{
  out << "[";
  ex_g->affiche(out);
  out << symbole();
  ex_d->affiche(out);
  out << "]";
  return out;
}
string Operation::symbole() const
{
  return symb_;
}

Addition::Addition(const Expression *ex_1, const Expression *ex_2) : Operation(ex_1, ex_2, " + ") {}

Addition *Addition::clone() const
{
  return new Addition(ex_g->clone(), ex_d->clone());
}

Addition *Addition::derive(const string &var) const
{
  return new Addition(ex_g->derive(var), ex_d->derive(var));
}
Expression *Addition::simplify() const
{
  ostringstream output1;
  ostringstream output2;

  Expression *simp_ex_g = ex_g->simplify();
  Expression *simp_ex_d = ex_d->simplify();

  output1 << *(simp_ex_g);
  if (output1.str() == "0")
  {
    return simp_ex_d;
  }

  output2 << *(simp_ex_d);
  if (output2.str() == "0")
  {
    return simp_ex_g;
  }

  return this->clone();
}
Multiplication::Multiplication(const Expression *ex_1, const Expression *ex_2) : Operation(ex_1, ex_2, " * "){};

Multiplication *Multiplication::clone() const
{
  return new Multiplication(ex_g->clone(), ex_d->clone());
}

Addition *Multiplication::derive(const string &var) const
{
  return new Addition(
      new Multiplication(ex_g->derive(var), ex_d->clone()),
      new Multiplication(ex_g->clone(), ex_d->derive(var)));
}
Expression *Multiplication::simplify() const
{
  ostringstream output1;
  ostringstream output2;

  Expression *simp_ex_g = ex_g->simplify();
  Expression *simp_ex_d = ex_d->simplify();
  output1 << *(simp_ex_g);
  output2 << *(simp_ex_d);

  if (output1.str() == "0" or output2.str() == "0")
  {
    return new Nombre();
  }
  if (output1.str() == "1")
  {
    return simp_ex_d;
  }
  if (output2.str() == "1")
  {
    return simp_ex_g;
  }
  return this->clone();
}
