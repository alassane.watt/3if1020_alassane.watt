#include <sstream>

#include "gtest/gtest.h"

#include "Expression.hpp"
using namespace std;

TEST(TestDerivation, TestAffichageNombre)
{
  ostringstream output;
  Nombre n(1);
  output << n;
  EXPECT_EQ(output.str(), "1");
}

TEST(TestDerivation, TestAffichageVariable)
{
  ostringstream output;
  const Expression *var = new Variable("x");
  output << *var;
  EXPECT_EQ(output.str(), "x");
  delete var;
}

TEST(TestDerivation, TestDerivationNombre)
{
  ostringstream output;
  const Expression *ex_1 = new Nombre(5);
  const Expression *ex_2 = ex_1->derive("x");
  ASSERT_TRUE(nullptr != ex_2);
  output << *ex_1;
  EXPECT_EQ(output.str(), "0");
  delete ex_1;
  delete ex_2;
}

TEST(TestDerivation, TestDerivationVariable)
{
  ostringstream output1;
  const Expression *ex_1 = new Variable("x");
  const Expression *ex_2 = ex_1->derive("x");
  ASSERT_TRUE(nullptr != ex_2);
  output1 << *ex_2;
  EXPECT_EQ(output1.str(), "1");
  delete ex_2;

  ostringstream output2;
  ex_2 = ex_1->derive("y");
  ASSERT_TRUE(nullptr != ex_2);
  output2 << *ex_2;
  EXPECT_EQ(output2.str(), "0");
  delete ex_1;
  delete ex_2;
}
TEST(TestDerivation, TestDestructionInstances)
{
  const Expression *ex_1 = new Nombre(5);
  const Expression *ex_2 = new Variable("x");
  ASSERT_TRUE(Expression::nb_instances_ == 2);
  delete ex_1;
  ASSERT_TRUE(Expression::nb_instances_ == 1);
  delete ex_2;
  ASSERT_TRUE(Expression::nb_instances_ == 0);
}

TEST(TestDerivation, TestAddition)
{
  ostringstream output1;
  ostringstream output2;
  ostringstream output3;
  const Expression *ex_1 = new Nombre(5);
  const Expression *ex_2 = new Variable("x");
  const Expression *ex_3 = new Addition(ex_1, ex_2);

  output1 << *ex_3;
  EXPECT_EQ(output1.str(), "(5 + x)");

  output2 << *(ex_3->derive("x"));
  EXPECT_EQ(output2.str(), "(0 + 1)");

  output3 << *(ex_3->derive("y"));
  EXPECT_EQ(output3.str(), "(0 + 0)");

  delete ex_1;
  delete ex_2;
  delete ex_3;
}

TEST(TestDerivation, TestDerivationMultiplication)
{
  ostringstream output;
  const Expression *ex_1 = new Multiplication(
      new Variable("x"),
      new Multiplication(new Variable("y"), new Variable("z")));
  const Expression *ex_2 = ex_1->derive("x");
  ASSERT_TRUE(nullptr != ex_2);

  output << *ex_2;
  EXPECT_EQ(output.str(), "((1 * (y * z)) + (x * ((0 * z) + (y * 0))))");
  delete ex_1;
  delete ex_2;
}

TEST(TestDerivation, TestSimplifierAddition)
{
  ostringstream output;
  const Expression *ex_1 = new Addition(new Variable("x"), new Nombre(0));
  const Expression *simp = ex_1->simplify();
  output << *simp;
  EXPECT_EQ(output.str(), "x");
  delete ex_1;
}

int main()
{

  const Expression *e = new Addition(
      new Addition(new Nombre(5), new Nombre(4)),
      new Multiplication(new Nombre(3), new Nombre(4)));
  cout << *e << endl;
  e->simplify();
  cout << *e << endl;
  return 0;
}
