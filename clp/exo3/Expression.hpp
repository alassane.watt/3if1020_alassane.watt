#ifndef EXPRESSION_H_
#define EXPRESSION_H_

#include <iostream>
#include <string>
using namespace std;

class Expression
{
public:
  Expression();
  virtual ~Expression();
  virtual Expression *derive(const string &n) const = 0;
  virtual ostream &affiche(ostream &out) const = 0;
  virtual Expression *clone() const = 0;
  virtual Expression *simplify() const;
  static int nb_instances_;
};

istream &operator>>(ostream &in, const Expression &expr);
ostream &operator<<(ostream &out, const Expression &expr);

class Nombre : public Expression
{
public:
  Nombre();
  Nombre(const int value);
  ostream &affiche(ostream &out) const override;
  Nombre *derive(const string &n) const override;
  Nombre *clone() const override;
  Nombre *simplify() const override;

private:
  const int value_;
};

class Variable : public Expression
{
public:
  Variable(string name);
  ostream &affiche(ostream &out) const override;
  Nombre *derive(const string &n) const override;
  Variable *clone() const override;
  Variable *simplify() const override;

private:
  const string name_;
};

class Operation : public Expression
{
public:
  Operation(const Expression *ex_1, const Expression *ex_2, string symb);
  ~Operation();
  string symbole() const;
  ostream &affiche(ostream &out) const override;

protected:
  const Expression *ex_g;
  const Expression *ex_d;

private:
  const string symb_;
};

class Addition : public Operation
{
public:
  Addition(const Expression *ex_1, const Expression *ex_2);
  Addition *derive(const string &n) const override;
  Addition *clone() const override;
  Expression *simplify() const override;
};

class Multiplication : public Operation
{
public:
  Multiplication(const Expression *ex_1, const Expression *ex_2);
  Addition *derive(const string &n) const override;
  Multiplication *clone() const override;
  Expression *simplify() const override;
};

#endif
