#include "Nombre.cpp"
#include "tests.cpp"
#include <iostream>
#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include <math.h>
#include "gtest/gtest.h"

int main(int argc, char **argv)
{
  Nombre n{01234567};
  unsigned int i{5};
  n *= i;
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
