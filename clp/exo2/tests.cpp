#include <utility>
#include <sstream>
#include "gtest/gtest.h"

// #include "Nombre.cpp"

TEST(TestNombre, TestNombreVide)
{
  Nombre n;
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "");
}

TEST(TestNombre, TestNombreNul)
{
  Nombre n{0};
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "0");
}

TEST(TestNombre, TestNombre12345678)
{
  Nombre n{12345678};
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestNombreAutre)
{
  Nombre n{12345678};
  Nombre autre{n};
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestNombreAffectation)
{
  Nombre n{12345678};
  Nombre autre;
  autre = n;
  n = *new Nombre(0);
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "0");
}

TEST(TestNombre, TestLectureGrandNombre)
{
  std::string big{"123456789123456789123456789123456789"};
  std::istringstream in{big};
  Nombre n;
  in >> n;
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), big);
}

TEST(TestNombre, TestNombreIncremente)
{
  Nombre n{12345698};
  unsigned int i{5};
  n += i;
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "12345703");
}

TEST(TestNombre, TestNombreDecremente)
{
  Nombre n{12345602};
  unsigned int i{5};
  n -= i;
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "12345597");
}

TEST(TestNombre, TestNombrePlus)
{
  Nombre r{12345698};
  Nombre n{};
  unsigned int i{5};
  n = r + i;
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "12345703");
}

TEST(TestNombre, TestNombreMoins)
{
  Nombre r{12345602};
  Nombre n{};
  unsigned int i{5};
  n = r - i;
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "12345597");
}

TEST(TestNombre, TestNombreMultEgal)
{
  Nombre n{12345698};
  unsigned int i{5};
  n *= i;
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "61728490");
}

TEST(TestNombre, TestNombreMult)
{
  Nombre r{12345698};
  Nombre n{};
  unsigned int i{5};
  n = r * i;
  std::ostringstream os;
  os << n;
  EXPECT_EQ(os.str(), "61728490");
}
