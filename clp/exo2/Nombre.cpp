#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include <math.h>

class Nombre
{
public:
  Nombre()
  {
    premier_ = nullptr;
  };

  Nombre(unsigned int n)
  {
    premier_ = new Chiffre(n);
  };

  Nombre(const Nombre &n)
  {
    premier_ = new Chiffre(*n.premier_);
  };

  Nombre &operator=(const Nombre &n)
  {
    if (this == &n)
    {
      return *this;
    }
    premier_ = n.premier_;
    return *this;
  };

  Nombre &operator+=(const unsigned int &n)
  {
    if (n == 0)
    {
      return *this;
    }
    premier_->increment(n);
    return *this;
  };

  Nombre &operator-=(const unsigned int &n)
  {
    if (n == 0)
    {
      return *this;
    }
    premier_->decrement(n);
    return *this;
  };

  Nombre &operator+(const unsigned int &n)
  {
    if (n == 0)
    {
      return *this;
    }
    static Nombre temp{*this};
    temp += n;
    return temp;
  };

  Nombre &operator-(const unsigned int &n)
  {
    if (n == 0)
    {
      return *this;
    }
    static Nombre temp{*this};
    temp -= n;
    return temp;
  };

  Nombre &operator*=(const unsigned int &n)
  {
    if (n == 1)
    {
      return *this;
    }
    premier_->mult(n);
    return *this;
  };

  Nombre &operator*(const unsigned int &n)
  {
    if (n == 1)
    {
      return *this;
    }
    static Nombre temp{*this};
    temp *= n;
    return temp;
  };

  ~Nombre(){};

  friend std::ostream &operator<<(std::ostream &out, const Nombre &nombre)
  {
    if (nombre.premier_ != nullptr)
    {
      nombre.premier_->fprint(out);
    }
    return out;
  };

  friend std::istringstream &operator>>(std::istringstream &input, Nombre &nombre)
  {
    unsigned int d;
    Nombre nombreCourant;
    Nombre nombreTemp;
    while (input.good())
    {
      int c{input.get()};
      if (std::isdigit(c))
      {
        unsigned int d{static_cast<unsigned int>(c - '0')};
        nombreTemp = nombreCourant;
        nombreCourant = *new Nombre(d);
        nombreCourant.premier_->suivant_ = nombreTemp.premier_;
      }
      else
        break;
    }
    nombre = *new Nombre(nombreCourant);
    return input;
  };

private:
  struct Chiffre
  {
    unsigned int chiffre_;
    Chiffre *suivant_ = nullptr;

    Chiffre(unsigned int n)
    {
      unsigned int unite = n % 10;
      chiffre_ = unite;
      if (n >= 10)
      {
        suivant_ = new Chiffre(n / 10);
      }
      else
      {
        suivant_ = nullptr;
      }
    }

    Chiffre(const Chiffre &c)
    {
      chiffre_ = c.chiffre_;
      if (c.suivant_)
      {
        suivant_ = new Chiffre(*c.suivant_);
      }
      else
      {
        suivant_ = nullptr;
      }
    }

    Chiffre &increment(const unsigned int &n)
    {
      if (n == 0)
      {
        return *this;
      }
      unsigned int sum = (n % 10) + chiffre_;
      if (sum < 10)
      {
        chiffre_ = sum;
        if (suivant_)
        {
          suivant_->increment(n / 10);
        }
        else
        {
          if (n / 10 != 0)
          {
            suivant_ = new Chiffre{n / 10};
          }
        }
      }
      else
      {
        chiffre_ = sum % 10;
        if (suivant_)
        {
          suivant_->increment(n / 10 + 1);
        }
        else
        {
          suivant_ = new Chiffre{n / 10};
        }
      }
      return *this;
    };

    Chiffre &decrement(const unsigned int &n)
    {
      if (n == 0)
      {
        return *this;
      }
      int sum = chiffre_ - (n % 10);
      if (sum >= 0)
      {
        chiffre_ = 10 - sum;
        suivant_->decrement(n / 10);
      }
      else
      {
        chiffre_ = 10 + (sum % 10);
        suivant_->decrement((n / 10) + 1);
      }
      return *this;
    };

    Chiffre &mult(const unsigned int &n)
    {
      if (n == 1)
      {
        return *this;
      }
      unsigned int multi = n * chiffre_;
      if (multi < 10)
      {
        chiffre_ = multi;
        if (suivant_)
        {
          suivant_->mult(n);
        }
      }
      else
      {
        chiffre_ = multi % 10;
        if (suivant_)
        {
          suivant_->mult(n);
          suivant_->increment(multi / 10);
        }
        else
        {
          if (multi / 10 > 0)
          {
            suivant_ = new Chiffre{multi / 10};
          }
        }
      }
      return *this;
    };

    void fprint(std::ostream &out)
    {
      if (suivant_)
      {
        suivant_->fprint(out);
      }
      out << chiffre_;
    }
  };
  Chiffre *premier_;
};
