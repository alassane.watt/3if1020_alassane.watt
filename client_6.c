#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

#define PORT 4455
bool running = true;
void stop_handler(int sig)
{
  printf("Inside STOP_HANDLER\n");
  printf("Numero sig recu: %d\n", sig);
  running = false;
};

void exit_message()
{
  printf("EXIT MESSAGE\n");
}

int main()
{
  srand(time(NULL));
  // atexit
  atexit(exit_message);

  // sigaction
  struct sigaction psa;
  psa.sa_handler = stop_handler;

  sigaction(SIGINT, &psa, NULL);
  sigaction(SIGTERM, &psa, NULL);
  sigaction(SIGPIPE, &psa, NULL);

  // socket
  int client_socket;
  struct sockaddr_in server_address;

  client_socket = socket(PF_INET, SOCK_STREAM, 0);
  memset(&server_address, '\0', sizeof(server_address));
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(PORT);
  server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
  connect(client_socket, (struct sockaddr *)&server_address, sizeof(server_address));

  int read_data[3];
  int r;

  // Start of loop message
  printf("Avant la boucle infinie du client.\n");
  while (running)
  {
    r = read(client_socket, &read_data, sizeof(read_data));

    if (r == -1)
    {
      printf("Reading error: -1\n");
      exit(EXIT_FAILURE);
    }
    else if (r == 0)
    {
      printf("Nothing to read (EOF): breaking out of the loop.\n");
      stop_handler(0);
    };
    printf("Process PID: %d\n", read_data[0]);
    printf("Process PPID: %d\n", read_data[1]);
    printf("Random number: %d\n", read_data[2]);
    sleep(1);
  }

  // End of loop message
  printf("Après la boucle infinie du client.\n");

  exit(EXIT_SUCCESS);
}
