#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>

bool running = true;
void stop_handler(int sig)
{
  printf("Inside STOP_HANDLER\n");
  printf("Numero sig recu: %d\n", sig);
  running = false;
};

void exit_message()
{
  printf("EXIT MESSAGE\n");
}

int main()
{
  srand(time(NULL));
  // atexit
  atexit(exit_message);

  // sigaction
  struct sigaction psa;
  psa.sa_handler = stop_handler;

  sigaction(SIGINT, &psa, NULL);
  sigaction(SIGTERM, &psa, NULL);

  // fork
  int f = fork();
  if (f == -1)
  {
    printf("Error forking.\n");
    exit(EXIT_FAILURE);
  }

  // waiting logic
  if (f != 0)
  {

    int status;
    if (wait(&status) == -1)
    {
      printf("Error trying to wait for child process.\n");
      exit(EXIT_FAILURE);
    }
    if (WIFEXITED(status))
    {
      const int es = WEXITSTATUS(status);
      printf("Child exit process number was: %d\n", es);
    }
    if (WIFSIGNALED(status))
    {
      const int es = WTERMSIG(status);
      printf("Child exit process number was (killed): %d\n", es);
      // psignal(WTERMSIG(status), "Exit signal");
    }
    if (WIFSTOPPED(status))
    {
      // psignal(WSTOPSIG(status), "Exit signal");
      const int es = WSTOPSIG(status);
      printf("Child exit process number was (stopped): %d\n", es);
    }
  }

  // logic
  printf("Avant la boucle infinie du process: %d\n", f);
  while (running)
  {
    printf("Process PID: %d\n", getpid());
    printf("Process PPID: %d\n", getppid());
    printf("Random number: %d\n", rand() % 100);
    sleep(1);
  }
  printf("Apres la boucle infinie du process: %d\n", f);

  exit(EXIT_SUCCESS);
}
