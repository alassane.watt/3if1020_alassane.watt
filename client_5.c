#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

bool running = true;
void stop_handler(int sig)
{
  printf("Inside STOP_HANDLER\n");
  printf("Numero sig recu: %d\n", sig);
  running = false;
};

void exit_message()
{
  printf("EXIT MESSAGE\n");
}

int main()
{
  srand(time(NULL));
  // atexit
  atexit(exit_message);

  // sigaction
  struct sigaction psa;
  psa.sa_handler = stop_handler;

  sigaction(SIGINT, &psa, NULL);
  sigaction(SIGTERM, &psa, NULL);
  sigaction(SIGPIPE, &psa, NULL);

  // fifo
  printf("Opening the fifo...\n");
  int fd = open("myfifo", O_RDONLY);
  if (fd == -1)
  {
    printf("Error opening the fifo\n");
    exit(EXIT_FAILURE);
  }

  int read_data[3];
  int r;

  // Start of loop message
  printf("Avant la boucle infinie du client.\n");
  while (running)
  {
    r = read(fd, &read_data, 3 * sizeof(int));

    if (r == -1)
    {
      printf("Reading error: -1\n");
      exit(EXIT_FAILURE);
    }
    else if (r == 0)
    {
      printf("Nothing to read (EOF): breaking out of the loop.\n");
      stop_handler(0);
    };
    printf("Process PID: %d\n", read_data[0]);
    printf("Process PPID: %d\n", read_data[1]);
    printf("Random number: %d\n", read_data[2]);
    sleep(1);
  }
  close(fd);

  // End of loop message
  printf("Après la boucle infinie du client.\n");

  exit(EXIT_SUCCESS);
}
