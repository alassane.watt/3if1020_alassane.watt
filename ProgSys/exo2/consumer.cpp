#include <iostream>
#include <sstream>
#include <random>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <boost/interprocess/shared_memory_object.hpp>
#include "classes.hpp"
#include <boost/interprocess/mapped_region.hpp>

int main()
{
  const unsigned box_size = 2;
  const unsigned nb_messages = 10;

  Random random(50);

  using namespace boost::interprocess;
  shared_memory_object shm_obj(open_only, "shared_memory", read_write);

  mapped_region region(shm_obj, read_write);
  MessageBox *box = static_cast<MessageBox *>(region.get_address());

  std::cout << "start" << std::endl;

  Consumer consumer(1, *box, random, nb_messages);

  std::thread consumer_thread(consumer);
  consumer_thread.join();

  std::cout << "finish" << std::endl;

  return 0;
}
