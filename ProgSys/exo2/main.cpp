#include <iostream>
#include <sstream>
#include <random>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <boost/interprocess/shared_memory_object.hpp>
#include "classes.hpp"
#include <boost/interprocess/mapped_region.hpp>

/*
 * Test avec plusieurs producteurs et consommateurs
 */
int main()
{
  using namespace boost::interprocess;

  const unsigned box_size = 2;
  const unsigned nb_messages = 10;

  Random random(50);
  //MessageBox box(box_size);
  shared_memory_object::remove("shared_memory");

  shared_memory_object shm_obj(create_only, "shared_memory", read_write);
  shm_obj.truncate(sizeof(MessageBox));

  mapped_region region(shm_obj, read_write);
  MessageBox *box = new (region.get_address()) MessageBox(box_size);

  std::cout << "start" << std::endl;
  Producer producer(1, *box, random, nb_messages);

  std::thread producer_thread(producer);
  producer_thread.join();

  std::cout << "finish" << std::endl;

  return 0;
}
