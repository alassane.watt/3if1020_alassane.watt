#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>

bool running = true;
void stop_handler(int sig)
{
  printf("Inside STOP_HANDLER\n");
  printf("Numero sig recu: %d\n", sig);
  running = false;
};

void exit_message()
{
  printf("EXIT MESSAGE\n");
}

int main()
{
  srand(time(NULL));
  // atexit
  atexit(exit_message);

  // sigaction
  struct sigaction psa;
  psa.sa_handler = stop_handler;

  sigaction(SIGINT, &psa, NULL);
  sigaction(SIGTERM, &psa, NULL);
  sigaction(SIGPIPE, &psa, NULL);

  // pipe
  int fd[2];
  if (pipe(fd) == -1)
  {
    printf("Error creating a pipe.\n");
    exit(EXIT_FAILURE);
  }

  // fork
  int f = fork();
  if (f == -1)
  {
    printf("Error forking\n");
    exit(EXIT_FAILURE);
  }

  // Start of loop message
  printf("Avant la boucle infinie du process: %d\n", f);

  // Communicating data between parent and child processes through a pipe
  if (f != 0)
  {
    close(fd[0]);
    int data[3] = {getpid(), getppid(), rand() % 100};
    while (running)
    {
      // printf("writing\n");
      if (write(fd[1], &data, 3 * sizeof(int)) == -1)
      {
        if (errno == EPIPE)
        {
          printf("Reading end in child process closed: breaking out of the loop with sigpipe handling.\n");
        }
        else
        {
          printf("Error writing on the file descriptor: exiting.\n");
          exit(EXIT_FAILURE);
        }
      };
      sleep(1);
    }
    close(fd[1]);

    int status;
    if (wait(&status) == -1)
    {
      printf("Error trying to wait for child process.\n");
      exit(EXIT_FAILURE);
    }
    if (WIFEXITED(status))
    {
      const int es = WEXITSTATUS(status);
      printf("Child exit process number was: %d\n", es);
    }
    if (WIFSIGNALED(status))
    {
      const int es = WTERMSIG(status);
      printf("Child exit process number was (killed): %d\n", es);
      // psignal(WTERMSIG(status), "Exit signal");
    }
    if (WIFSTOPPED(status))
    {
      // psignal(WSTOPSIG(status), "Exit signal");
      const int es = WSTOPSIG(status);
      printf("Child exit process number was (stopped): %d\n", es);
    }
  }

  else
  {
    close(fd[1]);
    int read_data[3];
    int r;
    while (running)
    {
      r = read(fd[0], &read_data, 3 * sizeof(int));
      if (r == -1)
      {
        printf("Reading error: -1\n");
        exit(EXIT_FAILURE);
      }
      else if (r == 0)
      {
        printf("Nothing to read (EOF): breaking out of the loop.\n");
        stop_handler(0);
      };
      printf("Process PID: %d\n", read_data[0]);
      printf("Process PPID: %d\n", read_data[1]);
      printf("Random number: %d\n", read_data[2]);
      sleep(1);
    }
    close(fd[0]);
  }

  // End of loop message
  printf("Après la boucle infinie du process: %d\n", f);

  exit(EXIT_SUCCESS);
}
