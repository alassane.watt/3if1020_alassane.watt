# 3IF1020_alassane.watt

1.1 ok

1.2 ok
Avec kill -s int le message est bien affiché.
Sans cette option le message n'est pas affiché.

Avec l'option -s KILL le message n'est pas affiché et on ne peut pas faire en sorte pour qu'il soit affiché.
Le terminal est supprimé pour -s KILL. sans cette option rien ne se passe sur le terminal bash. Cependant sur vscode le terminal est arreté et supprimé.

Sans modifier la variable running, le processus continue son cours pour les commandes kill et Ctrl-C mais pour le kill -9 le processus s'estompe.

fin 1.2

1.3 ok
atexit affiche le message de sortie pour les commande Ctrl-C et kill tout court mais pas kill -9.
fin 1.3

2.1 ok
Q) Comment peut-on distinguer les messages du père de ceux du fils ?
En utilisant l'id retourné par la fonction <fork>.
Fork retourne:

- 0 pour le processus fils
- le PID du processus fils pour le processus parent

On peut distinguer les messages du père et du fils en ajoutant le int retourné par la fonction fork.
Ctrl-C arrete les deux processus.

Q) Utiliser kill pour arrêter le processus fils puis ps a de nouveau : que remarquez-vous ?
Le processus parent continue toujours. Le processus fils est arrété mais toujours apparent avec des parentheses autour du nom de la commande (d'ailleurs pourquoi ?).
Q) Tuer le père, relancer votre programme et cette fois arrêter le père avec kill. Quels sont les changements visibles ?
En tuant le parent, seul ce processus est arrété. Le processus fils quant à lui continue mais son processus parent est affecté a un nouveau processus parent de pid 1. (i.e Le PPID du fils devient 1).Dans ce dernier cas, un ctrl-c ne pourra pas tuer le process fils; seul un kill y arrivera.

Q) Comparer sa valeur selon la façon dont vous arrêter le fils.
Pour pouvoir observer differentes valeurs de sortie du child process il faut eliminer la gestion du signal SIGTERM. Ainsi les macros WIFSIGNALED et WIFSTOPPED pourront etre vrai en fonction de la methode utilisé pour arreter le processus fils. Si le fils est arreté par un kill, WIFSIGNALED est vrai et alors un nombre different de 0 est retourné.

De maniere generale
La valeur de retour du child process est:

- si le programme se termine normalement: 0
- sinon: different de 0

fin 2.1

2.2

fin 2.2
