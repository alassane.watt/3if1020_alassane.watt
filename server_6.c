#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

#define PORT 4455
bool running = true;
void stop_handler(int sig)
{
  printf("Inside STOP_HANDLER\n");
  printf("Numero sig recu: %d\n", sig);
  running = false;
};

void exit_message()
{
  printf("EXIT MESSAGE\n");
}

int main()
{
  srand(time(NULL));
  // atexit
  atexit(exit_message);

  // sigaction
  struct sigaction psa;
  psa.sa_handler = stop_handler;

  sigaction(SIGINT, &psa, NULL);
  sigaction(SIGTERM, &psa, NULL);
  sigaction(SIGPIPE, &psa, NULL);

  // socket
  int sockfd;
  struct sockaddr_in server_address;

  int new_socket;
  struct sockaddr_in new_address;

  socklen_t addr_size;

  sockfd = socket(PF_INET, SOCK_STREAM, 0);
  memset(&server_address, '\0', sizeof(server_address));
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(PORT);
  server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
  bind(sockfd, (struct sockaddr *)&server_address, sizeof(server_address));

  listen(sockfd, 5);
  addr_size = sizeof(new_address);

  new_socket = accept(sockfd, (struct sockaddr *)&new_address, &addr_size);
  int data[3] = {getpid(), getppid(), rand() % 100};

  // Start of loop message
  printf("Avant la boucle infinie du serveur.\n");
  while (running)
  {
    printf("writing\n");
    if (write(new_socket, data, sizeof(data)) == -1)
    {
      if (errno == EPIPE)
      {
        printf("Reading end closed: breaking out of the loop with sigpipe handling.\n");
      }
      else
      {
        printf("Error writing on the file descriptor: exiting.\n");
        exit(EXIT_FAILURE);
      }
    };
    sleep(1);
  }

  // End of loop message
  printf("Après la boucle infinie du serveur.\n");

  exit(EXIT_SUCCESS);
}
