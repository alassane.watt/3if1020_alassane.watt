#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>

bool running = true;
void stop_handler(int sig)
{
  printf("Inside STOP_HANDLER\n");
  printf("Numero sig recu: %d\n", sig);
  running = false;
};

void exit_message()
{
  printf("EXIT MESSAGE\n");
}

int main()
{
  srand(time(NULL));
  // atexit
  atexit(exit_message);

  // sigaction
  struct sigaction psa;
  psa.sa_handler = stop_handler;

  sigaction(SIGINT, &psa, NULL);
  sigaction(SIGTERM, &psa, NULL);

  // logic
  printf("Avant la boucle infinie \n");
  while (running)
  {
    printf("Process PID: %d\n", getpid());
    printf("Process PPID: %d\n", getppid());
    printf("Random number: %d\n", rand() % 100);
    sleep(1);
  }
  printf("Après la boucle infinie \n");

  exit(EXIT_SUCCESS);
}
