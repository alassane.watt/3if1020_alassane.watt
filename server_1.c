#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
  printf("Avant la boucle infinie \n");
  while (1)
  {
    printf("Process PID: %d\n", getpid());
    printf("Process PPID: %d\n", getppid());
    printf("Random number: %d\n", rand() % 100);
    sleep(1);
  }
  printf("Après la boucle infinie \n");

  exit(EXIT_SUCCESS);
}